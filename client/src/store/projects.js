import Vue from 'vue';
import HTTP from '@/http';

export default {
    namespaced: true,
    state: {
        project: null,
        projects: [],
        newProjectName: null,
    },
    actions: {
        createProject({ commit, state }) {
            return HTTP()
                .post('/projects', {
                    title: state.newProjectName,
                })
                .then(({ data }) => {
                    commit('appendProject', data);
                    commit('setNewProjectName', null);
                });
        },
        fetchProjects({ commit }) {
            return HTTP()
                .get('/projects')
                .then(({ data }) => {
                    commit('setProjects', data);
                });
        },
        saveProject({ commit }, project) {
            return HTTP()
                .patch(`/projects/${project.id}`, project)
                .then(() => {
                    commit('unsetEditMode', project);
                });
        },
        deleteProject({ commit }, project) {
            return HTTP()
                .delete(`/projects/${project.id}`)
                .then(() => {
                    commit('removeProject', project);
                });
        },
    },
    getters: {},
    mutations: {
        setNewProjectName(state, name) {
            state.newProjectName = name;
        },
        setProjects(state, projects) {
            state.projects = projects;
        },
        setProjectTitle(state, { project, title }) {
            project.title = title;
        },
        appendProject(state, project) {
            state.projects.push(project);
        },
        setEditMode(state, project) {
            Vue.set(project, 'isEditMode', true);
        },
        unsetEditMode(state, project) {
            Vue.set(project, 'isEditMode', false);
        },
        removeProject(state, project) {
            state.projects.splice(state.projects.indexOf(project), 1);
        },
        setCurrentProject(state, project) {
            state.project = project;
        },
    },
};
