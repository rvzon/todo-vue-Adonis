import HTTP from '@/http';
import router from '@/router';

export default {
    namespaced: true,
    state: {
        registerEmail: null,
        registerPassword: null,
        registerError: null,
        loginEmail: null,
        loginPassword: null,
        loginError: null,
        token: null,
    },
    actions: {
        register({ commit, state }) {
            commit('setRegisterError', null);

            return HTTP()
                .post('/auth/register', {
                    email: state.registerEmail,
                    password: state.registerPassword,
                })
                .then(({ data }) => {
                    commit('setToken', data.token);
                    router.push('/');
                })
                .catch(() => {
                    commit('setRegisterError', 'Invalid registration information');
                });
        },
        login({ commit, state }) {
            commit('setLoginError', null);

            return HTTP()
                .post('/auth/login', {
                    email: state.loginEmail,
                    password: state.loginPassword,
                })
                .then(({ data }) => {
                    commit('setToken', data.token);
                    router.push('/');
                })
                .catch(() => {
                    commit('setLoginError', 'Invalid email/password');
                });
        },
        logout({ commit }) {
            commit('setToken', null);
            router.push('/login');
        },
    },
    getters: {
        isLoggedIn(state) {
            return !!state.token;
        },
    },
    mutations: {
        setRegisterEmail(state, email) {
            state.registerEmail = email;
        },
        setRegisterPassword(state, password) {
            state.registerPassword = password;
        },
        setToken(state, token) {
            state.token = token;
        },
        setRegisterError(state, error) {
            state.registerError = error;
        },
        setLoginEmail(state, email) {
            state.loginEmail = email;
        },
        setLoginPassword(state, password) {
            state.loginPassword = password;
        },
        setLoginError(state, error) {
            state.loginError = error;
        },
    },
};
